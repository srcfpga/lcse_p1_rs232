----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/12/2021 08:40:10 PM
-- Design Name:
-- Module Name: ShiftRegister_tb - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- use IEEE.NUMERIC_STD.ALL;
use std.env.finish;

entity ShiftRegister_tb is
end ShiftRegister_tb;

architecture testbench of ShiftRegister_tb is

	signal  Clk    : std_logic;
	signal  Reset  : std_logic;
	signal  Enable : std_logic;
	signal  D      : std_logic;
	signal  Q      : std_logic_vector(7 downto 0);


	component ShiftRegister
	port (
		Clk    : in  std_logic;
		Reset  : in  std_logic;
		Enable : in  std_logic;
		D      : in  std_logic;
		Q      : out std_logic_vector(7 downto 0)
	);
	end component;

	constant clk_period : time := 50 ns;
	constant clk_cycle  : time := 2*clk_period;

	procedure send_byte(
		signal   en:      out std_logic;
		signal   channel: out std_logic;
		constant byte:    in  std_logic_VECTOR(7 downto 0)
	) is
	begin
		en <= '1', '0' after 8*clk_cycle;
		FOR I IN 0 TO 7 LOOP
			channel <= byte(I);
			wait for clk_cycle;
		END LOOP;
		channel <= '0';
	end procedure send_byte;
begin

UUT: ShiftRegister
        port map (
		 Clk     => Clk,
		 Reset   => Reset ,
		 Enable  => Enable,
		 D       => D,
		 Q       => Q
        );

        -- Reloj
	process
	begin
		Clk <= '1', '0' after clk_period;
		wait for clk_cycle;
	end process;

	-- Reset
	process
	begin
		Reset <= '1', '0' after clk_cycle,
			 '1' after 2*clk_cycle;
		wait;
	end process;

	-- Send Data
	process
	begin

		Enable <= '0';
		D <= '0';
		wait for 3*clk_cycle;
		send_byte(Enable, D, x"aa");
		wait for 6*clk_cycle;
		send_byte(Enable, D, x"ff");
		wait for 6*clk_cycle;
		send_byte(Enable, D, x"ae");
		wait for 6*clk_cycle;
		send_byte(Enable, D, x"22");
		wait for 6*clk_cycle;
		send_byte(Enable, D, x"91");
		wait for 4*clk_cycle;
		report "Calling 'finish'"; finish;
	end process;

end testbench;
