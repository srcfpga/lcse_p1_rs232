----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/10/2021 04:39:20 PM
-- Design Name:
-- Module Name: ShiftRegister - Behavioural
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY ShiftRegister IS
	PORT (
	      Clk    : IN  STD_LOGIC;
	      Reset  : IN  STD_LOGIC;
	      Enable : IN  STD_LOGIC;
	      D      : IN  STD_LOGIC;
	      Q      : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
);
END ShiftRegister;
ARCHITECTURE Behavioural OF ShiftRegister IS
	SIGNAL Q_tmp : STD_LOGIC_VECTOR(7 DOWNTO 0);
BEGIN
	PROCESS (Clk, Reset)
	BEGIN
		IF Reset = '0' THEN
			Q_tmp <= (others => '0');
		ELSIF Clk'event AND Clk='1' THEN
			IF Enable = '1' THEN
				Q_tmp <= D & Q_tmp(7 downto 1);
			END IF;
		END IF;
	END PROCESS;
	Q <= Q_tmp;
END Behavioural;
