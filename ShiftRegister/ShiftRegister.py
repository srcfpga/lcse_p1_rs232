from vcd2json import WaveExtractor

path_list = [
        'shiftregister_tb/clk',
        'shiftregister_tb/uut/reset',
        'shiftregister_tb/uut/enable',
        'shiftregister_tb/uut/d',
        'shiftregister_tb/uut/q[7:0]',
        ]

extractor = WaveExtractor('ShiftRegister.vcd', 'ShiftRegister.json', path_list)
extractor.wave_chunk = 14
#  extractor.start_time = 0
#  extractor.end_time = 1.2e9
extractor.wave_format('shiftregister_tb/uut/q[7:0]', 'x')
extractor.execute()
