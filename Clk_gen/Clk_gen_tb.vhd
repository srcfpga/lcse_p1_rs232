----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/12/2021 08:40:10 PM
-- Design Name:
-- Module Name: Clk_gen_tb - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- use IEEE.NUMERIC_STD.ALL;
use std.env.finish;

entity Clk_gen_tb is
end Clk_gen_tb;

architecture testbench of Clk_gen_tb is

	SIGNAL reset    : IN  STD_LOGIC;
	SIGNAL clk_in1  : IN  STD_LOGIC;
	SIGNAL clk_out1 : OUT STD_LOGIC;
	SIGNAL locked   : OUT STD_LOGIC;


	component Clk_gen
		PORT (
		     reset     : IN  STD_LOGIC;
		     clk_in1   : IN  STD_LOGIC;
		     clk_out1  : OUT  STD_LOGIC;
		     locked    : OUT STD_LOGIC);
	end component;

	constant clk_period     : time    := 50 ns;
	constant clk_cycle      : time    := 2*clk_period;

	signal  baud   : std_logic;
begin

UUT: Clk_gen
        port map (
		 reset    => reset,
		 clk_in1  => clk_in1,
		 clk_out1 => clk_out1,
		 locked   => locked
        );


        -- Reloj
	process
	begin
		clk_in1<= '1', '0' after clk_period;
		wait for clk_cycle;
	end process;


	-- Reset
	process
	begin
		Reset <= '1', '0' after clk_cycle,
			 '1' after 2*clk_cycle;
		wait;
	end process;

	-- Send Data
	process
	begin

		wait for 10000 ns;
		report "Calling 'finish'"; finish;
	end process;

end testbench;
