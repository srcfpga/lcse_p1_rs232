----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/10/2021 04:39:20 PM
-- Design Name:
-- Module Name: Clk_gen - Behavioural
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY Clk_gen IS
      PORT (
      reset     : IN  STD_LOGIC;
      clk_in1   : IN  STD_LOGIC;
      clk_out1  : OUT  STD_LOGIC;
      locked    : OUT STD_LOGIC);
END Clk_gen;

ARCHITECTURE Behavioural OF Clk_gen IS
	SIGNAL baud_counter : INTEGER := 0;
BEGIN
	-- Registro de estado y salidas registradas
	PROCESS (clk_in1)
	BEGIN
		IF reset = '1' THEN
			baud_counter <= 0;
			locked <= '0';
		ELSIF clk_in1'event AND clk_in1='1' THEN
			IF (baud_counter = 868) THEN
				baud_counter <= 0;
				clk_out1 <= '1';
			ELSE
				baud_counter <= baud_counter + 1;
				clk_out1 <= '0';
			END IF;
		END IF;
	END PROCESS;
END Behavioural;
