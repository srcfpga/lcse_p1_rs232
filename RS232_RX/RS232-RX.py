from vcd2json import WaveExtractor

path_list = [
        'rs232_rx_tb/baud_clk',
        'rs232_rx_tb/uut/reset',
        'rs232_rx_tb/uut/linerd_in',
        'rs232_rx_tb/uut/valid_out',
        'rs232_rx_tb/uut/code_out',
        'rs232_rx_tb/uut/store_out',
        ]

extractor = WaveExtractor('RS232-RX.vcd', 'RS232-RX.json', path_list)
extractor.wave_chunk = 15
extractor.execute()
