----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/10/2021 04:39:20 PM
-- Design Name:
-- Module Name: RS232_RX - Behavioural
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY RS232_RX IS
	PORT (
		     Clk       : IN  STD_LOGIC;
		     Reset     : IN  STD_LOGIC;
		     LineRD_in : IN  STD_LOGIC;
		     Valid_out : OUT STD_LOGIC;
		     Code_out  : OUT STD_LOGIC;
		     Store_out : OUT STD_LOGIC
	     );
END RS232_RX;

ARCHITECTURE Behavioural OF RS232_RX IS
	TYPE state IS (Idle, StartBit, Receive, StopBit);
	SIGNAL current_state, next_state : state;
	SIGNAL baud_cnt_tmp,  baud_cnt   : UNSIGNED (7 DOWNTO 0);
	SIGNAL data_cnt_tmp,  data_cnt   : UNSIGNED (3 DOWNTO 0);
	CONSTANT baud   : INTEGER := 174;
	CONSTANT sample : INTEGER := 87;

BEGIN
	-- Outputs
	Valid_out <= '1' WHEN (baud_cnt = sample) AND (current_state = Receive) ELSE '0';
	Store_out <= '1' WHEN (baud_cnt = sample) AND (current_state = StopBit) ELSE '0';
	Code_out  <= LineRD_in WHEN (current_state = Receive) ELSE '0';
	-- Next-State Logic
	PROCESS (current_state, LineRD_in, baud_cnt_tmp, data_cnt_tmp, baud_cnt, data_cnt)
	BEGIN
	    baud_cnt_tmp <= baud_cnt;
		data_cnt_tmp <= data_cnt;
		next_state <= current_state;
		CASE current_state IS
			WHEN Idle =>
				IF (LineRD_in = '0') THEN
					next_state <= StartBit;
				ELSE
					next_state <= Idle;
				END IF;
			WHEN StartBit=>
				IF (baud_cnt = baud) THEN
					baud_cnt_tmp <= (OTHERS => '0');
					next_state   <= Receive;
				ELSE
					baud_cnt_tmp <= baud_cnt + 1;
					next_state   <= StartBit;
				END IF;
			WHEN Receive =>
				IF (baud_cnt = baud) THEN
					IF (data_cnt = 8) THEN
						next_state <= StopBit;
					ELSE
						next_state <= Receive;
					END IF;
					baud_cnt_tmp <= (OTHERS => '0');
				ELSE
					baud_cnt_tmp <= baud_cnt + 1;
					next_state   <= Receive;
				END IF;
				IF (baud_cnt = sample) THEN
					data_cnt_tmp <= data_cnt + 1;
				ELSE
					data_cnt_tmp <= data_cnt;
				END IF;
			WHEN StopBit =>
				data_cnt_tmp <= (OTHERS => '0');
				IF (baud_cnt = baud) THEN
					baud_cnt_tmp <= (OTHERS => '0');
					next_state   <= Idle;
				ELSE
					baud_cnt_tmp <= baud_cnt + 1;
					next_state   <= StopBit;
				END IF;
		END CASE;
	END PROCESS;
	PROCESS (Clk, Reset)
	BEGIN
		IF Reset = '0' THEN
			current_state <= Idle;
			data_cnt <= (others => '0');
			baud_cnt <= (others => '0');
		ELSIF Clk'event AND Clk='1' THEN
			current_state <= next_state;
			data_cnt <= data_cnt_tmp;
			baud_cnt <= baud_cnt_tmp;
		END IF;
	END PROCESS;
END Behavioural;
