----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/12/2021 08:40:10 PM
-- Design Name:
-- Module Name: RS232_RX_tb - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use std.env.finish;

entity RS232_RX_tb is

end RS232_RX_tb;

architecture testbench of RS232_RX_tb is

	signal Clk        : std_logic;
	signal Reset      : std_logic;
	signal LineRD_in  : std_logic;
	signal Valid_out  : std_logic;
	signal Code_out   : std_logic;
	signal Store_out  : std_logic;

	component RS232_RX
	port (
	      Clk       : in  std_logic;
	      Reset     : in  std_logic;
	      LineRD_in : in  std_logic;
	      Valid_out : out std_logic;
	      Code_out  : out std_logic;
	      Store_out : out std_logic
	);
	end component;

	constant clk_period : time := 50  ns;
	constant clk_cycle  : time := 2*clk_period;
	constant cycle_per_baud  : integer := 174;
	signal baud_clk : std_logic;

 	procedure send_byte(
		signal   channel: out std_logic;
		constant byte:    in  std_logic_VECTOR(7 downto 0)

	) is
	begin

		channel <= '0'; -- start bit
		wait for clk_cycle*cycle_per_baud;
		FOR I IN 7 DOWNTO 0 LOOP
			channel <= byte(I);
			wait for clk_cycle*cycle_per_baud;
		END LOOP;
		channel <= '1'; -- default
	end procedure send_byte;

begin

    UUT: RS232_RX
        port map (
		 Clk        => Clk,
		 Reset      => Reset,
		 LineRD_in  => LineRD_in,
		 Valid_out  => Valid_out,
		 Code_out   => Code_out,
		 Store_out  => Store_out
        );

    -- Reloj
    process
    begin
        baud_clk <= '1', '0' after clk_period*cycle_per_baud;
        wait for clk_cycle*cycle_per_baud;
    end process;

    process
    begin
        Clk <= '1', '0' after clk_period;
        wait for clk_cycle;
    end process;

    -- Reset
    process
    begin
	Reset <= '0', '1' after clk_cycle, '0' after clk_cycle*cycle_per_baud,
		 '1' after 2*clk_cycle*cycle_per_baud;
	wait;
    end process;

    -- Entrada de Datos
    process
    begin

	    LineRD_in <= '1'; -- Default

	    wait for 3*clk_cycle*cycle_per_baud;
	    send_byte(LineRD_in, x"aa");

	    wait for 6*clk_cycle*cycle_per_baud;
	    send_byte(LineRD_in, x"3d");

	    wait for 6*clk_cycle*cycle_per_baud;
	    send_byte(LineRD_in, x"f1");

	    wait for 6*clk_cycle*cycle_per_baud;

	    send_byte(LineRD_in, x"22");
	    wait for 3*clk_cycle*cycle_per_baud;
	    finish;
    end process;

end testbench;
