// File RS232-RX.vhd translated with vhd2vl v3.0 VHDL to Verilog RTL translator
// vhd2vl settings:
//  * Verilog Module Declaration Style: 2001

// vhd2vl is Free (libre) Software:
//   Copyright (C) 2001 Vincenzo Liguori - Ocean Logic Pty Ltd
//     http://www.ocean-logic.com
//   Modifications Copyright (C) 2006 Mark Gonzales - PMC Sierra Inc
//   Modifications (C) 2010 Shankar Giri
//   Modifications Copyright (C) 2002-2017 Larry Doolittle
//     http://doolittle.icarus.com/~larry/vhd2vl/
//   Modifications (C) 2017 Rodrigo A. Melo
//
//   vhd2vl comes with ABSOLUTELY NO WARRANTY.  Always check the resulting
//   Verilog for correctness, ideally with a formal verification tool.
//
//   You are welcome to redistribute vhd2vl under certain conditions.
//   See the license (GPLv2) file included with the source for details.

// The result of translation follows.  Its copyright status should be
// considered unchanged from the original VHDL.

//--------------------------------------------------------------------------------
// Company:
// Engineer:
//
// Create Date: 09/10/2021 04:39:20 PM
// Design Name:
// Module Name: RS232_RX - Behavioural
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//--------------------------------------------------------------------------------
// no timescale needed

module RS232_RX(
input wire Clk,
input wire Reset,
input wire LineRD_in,
output reg Valid_out,
output reg Code_out,
output reg Store_out
);




parameter [1:0]
  Idle = 0,
  StartBit = 1,
  Receive = 2,
  StopBit = 3;

reg [1:0] Current_State;
reg Valid_out_tmp; reg Store_out_tmp; reg Code_out_tmp;
reg [31:0] BitCounter = 0;
reg [31:0] baud_counter = 0;
reg baud_clk;

  always @(Current_State, LineRD_in) begin
    case(Current_State)
    Idle : begin
      Valid_out_tmp <= 1'b1;
      Store_out_tmp <= 1'b0;
      Code_out_tmp <= 1'b1;
    end
    StartBit : begin
      Valid_out_tmp <= 1'b0;
      Code_out_tmp <= 1'b1;
    end
    Receive : begin
      Valid_out_tmp <= 1'b0;
      Code_out_tmp <= LineRD_in;
    end
    StopBit : begin
      Valid_out_tmp <= 1'b1;
      Code_out_tmp <= 1'b1;
      Store_out_tmp <= 1'b1;
    end
    endcase
  end

  // Generador de Baudios
  always @(posedge Clk, posedge Reset) begin
    if(Reset == 1'b0) begin
      baud_counter <= 0;
      baud_clk <= 1'b0;
    end else begin
      if((baud_counter == 174)) begin
        baud_counter <= 0;
        baud_clk <= 1'b1;
      end
      else if((LineRD_in == 1'b0 && Current_State == Idle)) begin
        baud_counter <=  -87;
        baud_clk <= 1'b1;
      end
      else begin
        baud_counter <= baud_counter + 1;
        baud_clk <= 1'b0;
      end
    end
  end

  // Registro de estado y salidas registradas
  always @(posedge baud_clk) begin
    if((Reset == 1'b0)) begin
      Valid_out <= 1'b1;
      Store_out <= 1'b0;
      Code_out <= 1'b1;
    end else begin
      Store_out <= Store_out_tmp;
      Valid_out <= Valid_out_tmp;
      Code_out <= Code_out_tmp;
      case(Current_State)
            // next-state logic
      Idle : begin
        if(LineRD_in == 1'b0) begin
          Current_State <= StartBit;
        end
        else begin
          Current_State <= Idle;
        end
      end
      StartBit : begin
        Current_State <= Receive;
      end
      Receive : begin
        if(BitCounter < 6) begin
          BitCounter <= BitCounter + 1;
          Current_State <= Receive;
        end
        else begin
          BitCounter <= 0;
          Current_State <= StopBit;
        end
      end
      StopBit : begin
        Current_State <= Idle;
      end
      endcase
    end
  end


endmodule
