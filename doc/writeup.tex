\documentclass{article}
\usepackage[spanish]{babel}
\usepackage[a4paper, top = 20mm, bottom=20mm, right=20mm, left=20mm]{geometry}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{rotating}
\usepackage[hidelinks]{hyperref}
\usepackage{tikz}
\usepackage{listings}
\setlength{\parindent}{3mm}
\setlength{\parskip}{5mm}
\linespread{1.65}
% \renewcommand{\figurename}{Fig.}

\title{\textbf{Práctica 1}\thanks{Master en Ingeniería de Sistemas
Electrónicos, Escuela Técnica Superior de Ingenieros de Telecomunicación,
Madrid, España.} \textbf{ RS-232}
\date{\today}
\author{
\href{mailto:ignacio.amat@upm.es}{Ignacio Amat}
\hspace{2cm}
\href{mailto:alejandro.martinez@upm.es}{Alejandro Martinez}
}
}

\lstdefinestyle{mystyle}{
	language=VHDL,
	commentstyle=\color{red},
	keywordstyle=\color{blue},
	morekeywords={STD_LOGIC, INTEGER, STD_LOGIC_VECTOR},
	numberstyle=\color{gray},
	basicstyle=\linespread{.8}\ttfamily\footnotesize,
	breakatwhitespace=false,
	breaklines=true,
	captionpos=b,
	keepspaces=true,
	numbers=left,
	showspaces=false,
	showstringspaces=false,
	showtabs=false,
	frame=single
}

\lstset{style=mystyle}
\begin{document}

\maketitle

\vspace{-3\baselineskip}
\section{Transmisión}

El funcionamiento del módulo de transmisión se muestra en el siguiente	diagrama
de tiempos, posteriormente se procede a describir los eventos que  se  muestran  y
se discute la implementación desarrollada.
\begin{figure}[h!]
	\centering
	\def\svgwidth{\textwidth}
	\input{RS232-TX.tex}
	\caption{Diagrama de tiempos de transmisión.}
	\label{fig:tx}
\end{figure}

\clearpage

En el diagrama de tiempos \hyperref[fig:tx]{\textbf{Fig.}~\ref*{fig:tx}} se ve
como primero entra el \texttt{Reset} asíncrono, después se escribe el dato de
entrada en \texttt{data[7:0]} (\texttt{0xAF}, en el primer caso), seguidamente
se pone a HIGH la señal de \texttt{Start}, que normalmente está LOW. Cuando
comienza la transmisión, nuestro módulo TX va leyendo \texttt{data[7:0]} y
transmitiendo en la línea de trasmisión (\texttt{TX}) cada uno de los bits en
serie. Mientras el módulo TX está transmitiendo datos la línea \texttt{EOT}
baja desde HIGH y se mantiene en LOW cuanto dure la transmisión. Además de
enviar los 8 bits que componen el dato, el módulo TX envía un primer bit de
comienzo LOW y un último bit de parada HIGH.

\vfill
\begin{figure}[h!]
\centering
	  \resizebox{.75\textwidth}{!}{
		\input{RS232-TX-net.tex}
		}
	\caption{Circuito de transmisión elaborado.}
	\label{fig:txc}
\end{figure}
\vfill

El código se muestra en el \hyperref[lst:tx]{\textbf{Lst.}~\ref*{lst:tx}}, con
el circuito desarrollado en la
\hyperref[fig:txc]{\textbf{Fig.}~\ref*{fig:txc}}. Se ha implementado mediante
dos procesos, uno asíncrono que escribe las salidas en variables internas
temporales y otro proceso síncrono que mantiene el registro de las salidas y la
lógica del estado siguiente. Hemos definido una señal interna,
`\texttt{BitCount}' que lleva la cuenta del bit de entrada leído, cuenta desde
0 hasta 7; de manera similar definimos otra señal interna,
`\texttt{Current\_State}' que toma valores `\texttt{Idle}',
`\texttt{StartBit}', `\texttt{Send}' y `\texttt{Stop}', lo cuatro posibles
estados definidos.
\vspace{\baselineskip}
\lstinputlisting[
	firstline = 22,
	label = lst:tx,
	caption = {RS232-TX.vhd}
]{../RS232_TX/RS232-TX.vhd}

\clearpage

\section{Recepción}

La lógica desarrollada en el módulo de recepción es casi idéntica a la
desarrollada anteriormente. Este módulo toma el dato en serie de la señal de
recepción \texttt{LineRD\_in} (anteriormente señal de transmisión \texttt{TX})
y lo redirige mediante la línea \texttt{Code\_Out} hacia el \textit{Shift
Register}.  Mientras el módulo de recepción está enviando datos al
\textit{Shift Register}, mantiene la señal \texttt{Valid\_Out} en LOW
(normalmente está en HIGH). El módulo de recepción dispara su máquina de
estados cuando la línea \texttt{Line\_in} pasa a LOW, el \texttt{StartBit}, que
indica que los siguiente bits en serie se corresponden con los datos.
Lógicamente el \texttt{StartBit} no es transmitido al \textit{Shift Register}
para ser almacenado. El código implementado se muestra en el
\hyperref[lst:tx]{\textbf{Lst.}~\ref*{lst:rx}}, el circuito elaborado en
\hyperref[fig:rxc]{\textbf{Fig.}~\ref*{fig:rxc}}.

\vspace{-.3\baselineskip}
\begin{figure}[h!]
	\centering
	\def\svgwidth{\textwidth}
	\input{RS232-RX.tex}
	\caption{Diagrama de tiempos de recepción.}
\end{figure}

\clearpage

\phantom{}\vspace{-\baselineskip}
\lstinputlisting[
	firstline = 22,
	label = lst:rx,
	caption = {RS232-RX.vhd}
]{../RS232_RX/RS232-RX.vhd}
\clearpage

\phantom{}\vfill
\begin{figure}[h!]
\centering
	  \resizebox{.9\textwidth}{!}{
		\input{RS232-RX-net.tex}
		}
	\caption{Circuito de recepción elaborado.}
	\label{fig:rxc}
\end{figure}
\vfill

\clearpage

\section{Shift Register}

El comportamiento del \textit{Shift Register} es el esperado,
va acumulando los bits en serie que llegan por \texttt{D} en una
variable interna \texttt{Q\_tmp}. Cuando se termina la transmisión
se escribe el byte en \texttt{Q[7:0]}. A continuación se muestra el
diagrama de tiempos, el código generado y el circuito elaborado.

\vfill
\begin{figure}[h!]
	\centering
	\def\svgwidth{\textwidth}
	\input{ShiftRegister.tex}
	\caption{Diagrama de tiempos de Shift Register.}
\end{figure}
\vfill

\clearpage

\phantom{}\vfill
\lstinputlisting[
	firstline = 22,
	caption = {ShiftRegister.vhd}
]{../ShiftRegister/ShiftRegister.vhd}
\vfill

\clearpage

\phantom{}\vfill
\begin{figure}[h!]
	\resizebox{\textwidth}{!}{
		\input{ShiftRegister-net.tex}
		}
	\centering
	\caption{Circuito de Shift Register elaborado.}
\end{figure}
\vfill

\clearpage

\section{Sistema Completo}

Una vez implementados y analizados los módulos por separados procedemos
a juntarlos en una prueba final. Usando el \textit{testbench} proporcionado
vemos como se genera  unaseñal en \texttt{TX}  y como se recibe en \texttt{RX}
para posteriormente ser acumulada en el \textit{Shift Register} y después
guardada en una memoria interna FIFO.


\begin{figure}[h!]
\centering
\includegraphics[width = \linewidth]{rs232.png}
\vspace{-1.6\baselineskip}
\caption{Simulación del sistema completo.}
\end{figure}


\end{document}
