// File FIFO.vhd translated with vhd2vl v3.0 VHDL to Verilog RTL translator
// vhd2vl settings:
//  * Verilog Module Declaration Style: 2001

// vhd2vl is Free (libre) Software:
//   Copyright (C) 2001 Vincenzo Liguori - Ocean Logic Pty Ltd
//     http://www.ocean-logic.com
//   Modifications Copyright (C) 2006 Mark Gonzales - PMC Sierra Inc
//   Modifications (C) 2010 Shankar Giri
//   Modifications Copyright (C) 2002-2017 Larry Doolittle
//     http://doolittle.icarus.com/~larry/vhd2vl/
//   Modifications (C) 2017 Rodrigo A. Melo
//
//   vhd2vl comes with ABSOLUTELY NO WARRANTY.  Always check the resulting
//   Verilog for correctness, ideally with a formal verification tool.
//
//   You are welcome to redistribute vhd2vl under certain conditions.
//   See the license (GPLv2) file included with the source for details.

// The result of translation follows.  Its copyright status should be
// considered unchanged from the original VHDL.

//--------------------------------------------------------------------------------
// Company:
// Engineer:
//
// Create Date: 09/10/2021 04:39:20 PM
// Design Name:
// Module Name: FIFO - Behavioural
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//--------------------------------------------------------------------------------
// no timescale needed

module FIFO(
input wire clk,
input wire srst,
input wire [7:0] din,
input wire wr_en,
input wire rd_en,
output reg [7:0] dout,
output wire full,
output wire empty
);




parameter [1:0]
  S0 = 0,
  S1 = 1,
  W0 = 2,
  R0 = 3;

reg [1:0] current_state; reg [1:0] next_state;

reg [7:0] buff[0:16];
reg [7:0] dout_tmp;
wire [31:0] I;

  always @(clk) begin : P1
    reg [31:0] count;

    // Por defecto
    next_state <= current_state;
    // reset síncrono
    if((srst == 1'b1)) begin
      next_state <= S0;
      dout_tmp <= 8'h00;
      count = 0;
      for (I=0; I <= 16; I = I + 1) begin
        buff[I] <= 8'h00;
      end
    end
    else begin
      case(current_state)
      S0 : begin
        if(wr_en == 1'b0) begin
          next_state <= W0;
        end
        else if(rd_en == 1'b0) begin
          next_state <= R0;
        end
        else begin
          next_state <= S0;
        end
      end
      W0 : begin
        for (I=16; I >= 1; I = I - 1) begin
          buff[I] <= buff[I - 1];
          // report "din " & to_hstring(din);
          // report "I " & integer'image(I);
        end
        if(count < 16) begin
          buff[0] <= din;
          count = count + 1;
        end
        // report "count " & integer'image(count);
        // report "buff " & to_hstring(buff(0));
        next_state <= S1;
      end
      R0 : begin
        // report "count " & integer'image(count);
        // report "dout " & to_hstring(buff(count));
        if(count > 0 && rd_en == 1'b0) begin
          count = count - 1;
          dout_tmp <= buff[count];
        end
        next_state <= S0;
      end
      S1 : begin
        next_state <= S0;
      end
      endcase
    end
  end

  // Registro de estado y salidas registradas
  always @(posedge clk) begin
    current_state <= next_state;
    dout <= dout_tmp;
  end


endmodule
