from vcd2json import WaveExtractor

path_list = [
        'fifo_tb/uut/clk',
        'fifo_tb/uut/srst',
        'fifo_tb/uut/wr_en',
        'fifo_tb/uut/din[7:0]',
        'fifo_tb/uut/rd_en',
        'fifo_tb/uut/dout[7:0]',
        ]

extractor = WaveExtractor('FIFO.vcd', 'FIFO.json', path_list)
extractor.wave_chunk = 16
#  extractor.start_time = 0
#  extractor.end_time = 1.2e9
#  extractor.wave_format('fifo_tb/uut/q[7:0]', 'x')
extractor.execute()
