----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/12/2021 08:40:10 PM
-- Design Name:
-- Module Name: FIFO_tb - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- use IEEE.NUMERIC_STD.ALL;

entity FIFO_tb is
end FIFO_tb;

architecture testbench of FIFO_tb is

	signal  clk   : std_logic;
	signal  srst  : std_logic;
	signal  din   : std_logic_VECTOR(7 downto 0);
	signal  wr_en : std_logic;
	signal  rd_en : std_logic;
        signal  dout  : std_logic_VECTOR(7 downto 0);
        signal  full  : std_logic;
        signal  empty : std_logic;

	component FIFO
	port (
	     clk   : IN  std_logic;
	     srst  : IN  std_logic;
	     din   : IN  std_logic_VECTOR(7 downto 0);
	     wr_en : IN  std_logic;
	     rd_en : IN  std_logic;
	     dout  : OUT std_logic_VECTOR(7 downto 0);
	     full  : OUT std_logic;
	     empty : OUT std_logic
	     );
	end component;

	constant clk_period : time := 50  ns;
	constant clk_cycle  : time := 2*clk_period;
	constant t1         : time := 400 ns;
	constant t2         : time := 1900 ns;

	-- Send Byte
	procedure send_byte(
		signal en: out std_logic;
		signal ou: out std_logic_VECTOR(7 downto 0);
		constant byte: in std_logic_VECTOR (7 downto 0)

	) is
	begin
		en <= '0', '1' after clk_cycle;
		ou <= byte;
		wait for 2*clk_cycle;
	end procedure send_byte;

	-- Read Byte
	procedure read_byte(
		signal rd: out std_logic

	) is
	begin
		rd <= '0', '1' after 2*clk_cycle;
		wait for 3*clk_cycle;
	end procedure read_byte;

begin

UUT: FIFO
	port map (
		 clk   => clk,
		 srst  => srst,
		 din   => din,
		 wr_en => wr_en,
		 rd_en => rd_en,
		 dout  => dout,
		 full  => full,
		 empty => empty
		 );


	-- Reloj
	process
	begin
		clk <= '1', '0' after clk_period;
		wait for clk_cycle;
	end process;

	-- Reset
	process
	begin
		srst <= '0', '1' after clk_cycle, '0' after 2*clk_cycle;
		wait;
	end process;

	-- Send
	process
	begin
		-- default
		wr_en <= '1';
		rd_en <= '1';
		din   <= x"00";

		wait for 4*clk_cycle;
		send_byte(wr_en, din, x"ff");
		send_byte(wr_en, din, x"0f");
		send_byte(wr_en, din, x"0e");
		send_byte(wr_en, din, x"0d");
		send_byte(wr_en, din, x"0c");
		send_byte(wr_en, din, x"0b");
		send_byte(wr_en, din, x"0a");
		send_byte(wr_en, din, x"09");
		send_byte(wr_en, din, x"08");
		send_byte(wr_en, din, x"07");
		send_byte(wr_en, din, x"06");
		send_byte(wr_en, din, x"05");
		send_byte(wr_en, din, x"04");
		send_byte(wr_en, din, x"03");
		send_byte(wr_en, din, x"02");
		send_byte(wr_en, din, x"01");
		-- send_byte(wr_en, din, x"e7");
		-- send_byte(wr_en, din, x"27");

		din <= x"00";

		wait for clk_cycle;

		-- read_byte(rd_en);
		-- read_byte(rd_en);
		-- read_byte(rd_en);
		FOR I IN 0 TO 16 LOOP
			read_byte(rd_en);
		END LOOP;

		wait;
	end process;

end testbench;
