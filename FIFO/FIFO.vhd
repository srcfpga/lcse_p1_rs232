----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/10/2021 04:39:20 PM
-- Design Name:
-- Module Name: FIFO - Behavioural
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY FIFO IS
	PORT (
	      clk   : IN  std_logic;
	      srst  : IN  std_logic;
	      din   : IN  std_logic_VECTOR(7 downto 0);
	      wr_en : IN  std_logic;
	      rd_en : IN  std_logic;
	      dout  : OUT std_logic_VECTOR(7 downto 0);
	      full  : OUT std_logic;
	      empty : OUT std_logic);
END FIFO;

ARCHITECTURE Behavioural OF FIFO IS

	TYPE state IS (S0, S1, W0, R0);
	SIGNAL current_state, next_state : state;


	TYPE memory is array (0 to 16) of std_logic_VECTOR(7 downto 0);
	SIGNAL buff : memory;

	SIGNAL dout_tmp : std_logic_vector(7 downto 0);
	SIGNAL I : INTEGER range 0 to 16;

BEGIN
	PROCESS (clk)
		VARIABLE count : INTEGER range 0 to 16;
	BEGIN
		-- Por defecto
		next_state <= current_state;

	       -- reset síncrono
		IF (srst = '1') THEN
			next_state <= S0;
			dout_tmp <= x"00";
			count := 0;
			for I in 0 to 16 loop
				buff(I) <= x"00";
			end loop;
		ELSE
			CASE current_state IS

				WHEN S0 =>
					IF wr_en = '0' THEN
						next_state <= W0;
					ELSIF rd_en = '0' THEN
						next_state <= R0;
					ELSE
						next_state <= S0;
					END IF;

				WHEN W0 =>
					FOR I IN 16 DOWNTO 1 LOOP
						buff(I) <= buff(I-1);
						-- report "din " & to_hstring(din);
						-- report "I " & integer'image(I);
					END LOOP;

					IF count < 16 THEN
						buff(0) <= din;
						count := count + 1;
					END IF;
					-- report "count " & integer'image(count);
					-- report "buff " & to_hstring(buff(0));
					next_state <= S1;

				WHEN R0 =>
					-- report "count " & integer'image(count);
					-- report "dout " & to_hstring(buff(count));
					IF count > 0 and rd_en = '0' THEN
						count := count - 1;
						dout_tmp <= buff(count);
					END IF;
					next_state <= S0;
				WHEN S1 =>
					next_state <= S0;
			END CASE;
		END IF;
	END PROCESS;

	-- Registro de estado y salidas registradas
	PROCESS (clk)
	BEGIN
		IF clk'event AND clk='1' THEN
			current_state <= next_state;
			dout <= dout_tmp;
		END IF;
	END PROCESS;

END Behavioural;
