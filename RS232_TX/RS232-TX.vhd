----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/10/2021 04:39:20 PM
-- Design Name:
-- Module Name: RS232_TX - Behavioural
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY RS232_TX IS
	PORT (
		     Clk   : IN  STD_LOGIC;
		     Reset : IN  STD_LOGIC;
		     Start : IN  STD_LOGIC;
		     Data  : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		     EOT   : OUT STD_LOGIC;
		     TX    : OUT STD_LOGIC
	     );
END RS232_TX;
ARCHITECTURE Behavioural OF RS232_TX IS
	TYPE state IS (Idle, StartBit, Send, Stop);
	SIGNAL current_state, next_state : state;
	SIGNAL baud_cnt_tmp, baud_cnt : UNSIGNED (7 DOWNTO 0);
	SIGNAL data_cnt_tmp, data_cnt : UNSIGNED (3 DOWNTO 0);
BEGIN
	TX  <= '1' WHEN (current_state = Idle) OR (current_state = Stop) ELSE
	       '0' WHEN (current_state = StartBit) ELSE
	       Data(to_integer(data_cnt));
	EOT <= '1' WHEN (current_state = Idle) ELSE '0';
	PROCESS (current_state, baud_cnt, data_cnt, Start)
	BEGIN -- Next-State Logic
		next_state   <= current_state;
		data_cnt_tmp <= data_cnt;
		baud_cnt_tmp <= baud_cnt;
		CASE current_state IS
			WHEN Idle =>
				IF Start = '1' THEN
					next_state <= StartBit;
				ELSE
					next_state <= Idle;
				END IF;
			WHEN StartBit =>
				baud_cnt_tmp <= baud_cnt + 1;
				IF (baud_cnt >= 174 ) then
					next_state <= Send;
					baud_cnt_tmp <= (OTHERS => '0');
					data_cnt_tmp <= (OTHERS => '0');
				ELSE
					next_state <= StartBit;
				END IF;
			WHEN Send =>
				baud_cnt_tmp <= baud_cnt + 1;
				IF (baud_cnt >= 174 ) THEN
					baud_cnt_tmp <= (OTHERS => '0');
					data_cnt_tmp <= data_cnt + 1;
					IF (data_cnt > 6) THEN
						next_state <= Stop;
						data_cnt_tmp <= (OTHERS => '0');
					ELSE
						next_state <= Send;
					END IF;
				ELSE
					next_state <= Send;
				END IF;
			WHEN Stop =>
				baud_cnt_tmp <= baud_cnt + 1;
				IF (baud_cnt >= 174 ) THEN
					next_state <= Idle;
					baud_cnt_tmp   <= (OTHERS => '0');
				ELSE
					next_state <= Stop;
				END IF;
		END CASE;
	END PROCESS;
	PROCESS (Clk, Reset)
	BEGIN
		IF (Reset = '0') THEN
			current_state <= Idle;
			baud_cnt <= (others => '0');
			data_cnt <= (others => '0');
		ELSIF Clk'event AND Clk='1' THEN
			current_state <= next_state;
			baud_cnt <= baud_cnt_tmp;
			data_cnt <= data_cnt_tmp;
		END IF;
	END PROCESS;
END Behavioural;
