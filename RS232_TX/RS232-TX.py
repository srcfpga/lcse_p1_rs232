from vcd2json import WaveExtractor

path_list = [
        'rs232_tx_tb/baud_clk',
        'rs232_tx_tb/reset',
        'rs232_tx_tb/start',
        'rs232_tx_tb/uut/data[7:0]',
        'rs232_tx_tb/uut/eot',
        'rs232_tx_tb/uut/tx',
        ]

extractor = WaveExtractor('RS232-TX.vcd', 'RS232-TX.json', path_list)
extractor.wave_chunk = 15
#  extractor.start_time = 0
#  extractor.end_time = 1.2e9
extractor.wave_format('rs232_tx_tb/uut/data[7:0]', 'x')
extractor.execute()
