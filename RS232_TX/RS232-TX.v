// File RS232-TX.vhd translated with vhd2vl v3.0 VHDL to Verilog RTL translator
// vhd2vl settings:
//  * Verilog Module Declaration Style: 2001

// vhd2vl is Free (libre) Software:
//   Copyright (C) 2001 Vincenzo Liguori - Ocean Logic Pty Ltd
//     http://www.ocean-logic.com
//   Modifications Copyright (C) 2006 Mark Gonzales - PMC Sierra Inc
//   Modifications (C) 2010 Shankar Giri
//   Modifications Copyright (C) 2002-2017 Larry Doolittle
//     http://doolittle.icarus.com/~larry/vhd2vl/
//   Modifications (C) 2017 Rodrigo A. Melo
//
//   vhd2vl comes with ABSOLUTELY NO WARRANTY.  Always check the resulting
//   Verilog for correctness, ideally with a formal verification tool.
//
//   You are welcome to redistribute vhd2vl under certain conditions.
//   See the license (GPLv2) file included with the source for details.

// The result of translation follows.  Its copyright status should be
// considered unchanged from the original VHDL.

//--------------------------------------------------------------------------------
// Company:
// Engineer:
//
// Create Date: 09/10/2021 04:39:20 PM
// Design Name:
// Module Name: RS232_TX - Behavioural
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//--------------------------------------------------------------------------------
// no timescale needed

module RS232_TX(
input wire Clk,
input wire Reset,
input wire Start,
input wire [7:0] Data,
output wire EOT,
output wire TX
);




parameter [1:0]
  Idle = 0,
  StartBit = 1,
  Send = 2,
  Stop = 3;

reg [1:0] current_state; reg [1:0] next_state;
reg [7:0] baud_cnt_tmp; reg [7:0] baud_cnt;
reg [3:0] data_cnt_tmp; reg [3:0] data_cnt;

  assign TX = (current_state == Idle) || (current_state == Stop) ? 1'b1 : (current_state == StartBit) ? 1'b0 : Data[data_cnt];
  assign EOT = (current_state == Idle) ? 1'b1 : 1'b0;
  always @(current_state, baud_cnt, data_cnt, Start) begin
    // Next-State Logic
    next_state <= current_state;
    data_cnt_tmp <= data_cnt;
    baud_cnt_tmp <= baud_cnt;
    case(current_state)
    Idle : begin
      if(Start == 1'b1) begin
        next_state <= StartBit;
      end
      else begin
        next_state <= Idle;
      end
    end
    StartBit : begin
      baud_cnt_tmp <= baud_cnt + 1;
      if((baud_cnt >= 174)) begin
        next_state <= Send;
        baud_cnt_tmp <= {8{1'b0}};
        data_cnt_tmp <= {4{1'b0}};
      end
      else begin
        next_state <= StartBit;
      end
    end
    Send : begin
      baud_cnt_tmp <= baud_cnt + 1;
      if((baud_cnt >= 174)) begin
        baud_cnt_tmp <= {8{1'b0}};
        data_cnt_tmp <= data_cnt + 1;
        if((data_cnt > 6)) begin
          next_state <= Stop;
          data_cnt_tmp <= {4{1'b0}};
        end
        else begin
          next_state <= Send;
        end
      end
      else begin
        next_state <= Send;
      end
    end
    Stop : begin
      baud_cnt_tmp <= baud_cnt + 1;
      if((baud_cnt >= 174)) begin
        next_state <= Idle;
        baud_cnt_tmp <= {8{1'b0}};
      end
      else begin
        next_state <= Stop;
      end
    end
    endcase
  end

  always @(posedge Clk, posedge Reset) begin
    if((Reset == 1'b0)) begin
      current_state <= Idle;
      baud_cnt <= {8{1'b0}};
      data_cnt <= {4{1'b0}};
    end else begin
      current_state <= next_state;
      baud_cnt <= baud_cnt_tmp;
      data_cnt <= data_cnt_tmp;
    end
  end


endmodule
