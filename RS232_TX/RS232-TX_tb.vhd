----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 09/12/2021 08:40:10 PM
-- Design Name:
-- Module Name: RS232_TX_tb - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use std.env.finish;

entity RS232_TX_tb is

end RS232_TX_tb;

architecture testbench of RS232_TX_tb is

	signal Clk   : std_logic;
	signal Reset : std_logic;
	signal Start : std_logic;
	signal Data  : std_logic_vector(7 downto 0);
	signal EOT   : std_logic;
	signal TX    : std_logic;

	component RS232_TX
	port (
	      Clk   : in  std_logic;
	      Reset : in  std_logic;
	      Start : in  std_logic;
	      Data  : in  std_logic_vector(7 downto 0);
	      EOT   : out std_logic;
	      TX    : out std_logic
	);
	end component;

	constant clk_period : time := 50  ns;
	constant clk_cycle  : time := 2*clk_period;
	constant cycle_per_baud  : integer := 174;
	signal baud_clk : std_logic;

	-- Send Byte
	procedure send_byte(
		signal   en:   out std_logic;
		signal   ou:   out std_logic_VECTOR(7 downto 0);
		constant byte: in  std_logic_VECTOR(7 downto 0)

	) is
	begin
		en <= '1', '0' after clk_cycle*cycle_per_baud;
		ou <= byte;
		wait until EOT = '1';
	end procedure send_byte;
begin

    UUT: RS232_TX
        port map (
		 Clk   => Clk,
		 Reset => Reset,
		 Start => Start,
		 Data  => Data,
		 EOT   => EOT,
		 TX    => TX
        );

    -- Reloj
    process
    begin
        baud_clk <= '1', '0' after clk_period*cycle_per_baud;
        wait for clk_cycle*cycle_per_baud;
    end process;

    process
    begin
        Clk <= '1', '0' after clk_period;
        wait for clk_cycle;
    end process;

    -- Reset
    process
    begin
	Reset <= '0', '1' after clk_cycle*cycle_per_baud;
	wait;
    end process;

    -- Entrada de Datos
    process
    begin
	    -- default
	    Data  <= x"00";
	    Start <= '0';
	    wait for 3*clk_cycle*cycle_per_baud;
	    send_byte(Start, Data, x"af");
	    wait for 5*clk_cycle*cycle_per_baud;
	    send_byte(Start, Data, x"e2");
	    wait for 5*clk_cycle*cycle_per_baud;
	    send_byte(start, data, x"22");
	    wait for 2*clk_cycle*cycle_per_baud;
	    finish;
    end process;
end testbench;
